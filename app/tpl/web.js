/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import {Header,Footer,Web} from '../common/Layout';
import {styles} from '../common/Css';
var {
    View,
    WebView

    } = React;



module.exports = React.createClass({
    getInitialState:function(){
        return {
        }
    },
    render: function() {
        //console.log(this.props);
        return (
            <View>
                <Header {...this.props} />
                <View style={[styles.content]}>

                    <Web {...this.props}/>

            </View>
                <Footer navigator={this.props.navigator} active={'Home'}/>

            </View>



        );

    },
});



