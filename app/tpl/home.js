/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import {Header,Footer,Layout} from '../common/Layout';
import ListBox from '../component/ListBox';
import {styles} from '../common/Css';
import Config from '../common/Config';
var {
    View,

    } = React;



module.exports = React.createClass({
    getInitialState:function(){
        return {
        }
    },
    render: function() {
        return (
            <View>
                <Header {...this.props} />
                <View style={[styles.content]}>
                        <ListBox  {...this.props} url={Config.jx}/>
                 </View>
                <Footer navigator={this.props.navigator} active={'Home'}/>

            </View>



        );

    },
});



